export const environment = {
  production: true,
  hmr: false,
  //baseUrl: 'http://localhost:3000/'
  baseUrl: 'http://appwtg.transestrella.com:8080/',
  token_auth_config: {
    apiBase: 'http://appwtg.transestrella.com:8080'
  }
  //baseUrl: 'https://grupowtg.herokuapp.com/'
  //baseUrl: 'http://wtg/'
}
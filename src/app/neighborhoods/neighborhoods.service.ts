import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/Rx';

@Injectable()
export class NeighborhoodsService {

  neighborhoodsURL: string =  environment.baseUrl + 'neighborhoods';

  constructor(private http: Http) { }

  newNeighborhood( neighborhood ) {
    const url = `${this.neighborhoodsURL}`;
    return this.http.post( url, neighborhood )
            .map( (res) => {
              return res.json();
            });
  }

  getNeighborhoods() {
    return this.http.get( this.neighborhoodsURL)
    .map( (res) => {
      return res.json();
    });
  }

  getNeighborhood(id) {
    const url = `${this.neighborhoodsURL}/${id}`;
    return this.http.get( url).map(
      (res) => res.json()
    );
  }


  updateNeighborhood(neighborhood) {
    const url = `${this.neighborhoodsURL}/${neighborhood.id}`;
    return this.http.put(url, {neighborhood}).map(
      (res) => res.json()
    );
  }

  deleteNeighborhood(id){
    const url = `${this.neighborhoodsURL}/delete_neighborhood`;
    return this.http.post( url, {id: id})
      .map( (res) => res );
  }

  searchByName(search_string: string) {
    const url = `${this.neighborhoodsURL}/search`;
    return this.http.get(url, {
      params: {
        search_string
      }
    }).map(
      (res) => {
        return res.json();
      }
    );
  }

}

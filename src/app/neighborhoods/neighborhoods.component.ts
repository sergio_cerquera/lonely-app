import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-neighborhoods',
  template: `<router-outlet></router-outlet>`
})
export class NeighborhoodsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

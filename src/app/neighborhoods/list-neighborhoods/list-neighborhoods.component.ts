import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { NeighborhoodsService } from '../neighborhoods.service'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-neighborhoods',
  templateUrl: './list-neighborhoods.component.html',
  styleUrls: ['./list-neighborhoods.component.css']
})
export class ListNeighborhoodsComponent implements OnInit {

	neighborhoods:any[] = []

	constructor(public snackBar: MatSnackBar,private router: Router,
		public _neighborhoodsService: NeighborhoodsService,private toastr: ToastrService) { 
		this.getNeighborhoods()
	}

	ngOnInit() {
	}

	getNeighborhoods(){
		this._neighborhoodsService.getNeighborhoods().subscribe(
	      (data) =>{ this.neighborhoods = data},
	      (error) => console.log(error)
	    );
	}

	deleteNeighborhood(id){
		if(confirm("Estas seguro que deseas borrar este Barrio?")) {
		  this._neighborhoodsService.deleteNeighborhood(id)
		  .subscribe( (data) => {
		    if (data["_body"] == "OK") {
				for (var i in this.neighborhoods) {
					if (this.neighborhoods[i].id == id) {
					  this.neighborhoods.splice(Number(i),1);
					}
				}
				this.toastr.success('Barrio Borrado', 'Completado!');
		    }else{
	    		this.toastr.error('No se pudo Borrar el Barrio', 'Error!');
		    }
		  });
		}
	}

}

import { Component,ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { NeighborhoodsService } from '../neighborhoods.service'
import { CitiesService } from '../../cities/cities.service'
import { ShowLoadingComponent } from '../../show-loading/show-loading.component'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-save-neighborhoods',
  templateUrl: './save-neighborhoods.component.html',
  styleUrls: ['./save-neighborhoods.component.css']
})
export class SaveNeighborhoodsComponent{

	@ViewChild(ShowLoadingComponent, {static: false}) hijo: ShowLoadingComponent;

	form_name:string;
	neighborhood:any = {};
	cities:any[] = []

	constructor(public snackBar: MatSnackBar, public _neighborhoodsService: NeighborhoodsService,
		private router: Router,private route: ActivatedRoute,
		private _citiesService: CitiesService,private toastr: ToastrService) {
		this.getCities();
		if (route.params["_value"].id == null) {
    	 	this.form_name = "Nuevo Barrio"
    	}else{
    		this.form_name = "Editar Barrio"
    		route.params.subscribe(
		      (parametros) => {
		        if (parametros['id']) {
		          	this.getNeighborhood(parametros['id']);
		        }
		      }
		    );
		} 
	}

	ngOnInit() {
	}

	getCities(){
		this._citiesService.getCities().subscribe(
	      (data) =>{ this.cities = data},
	      (error) => console.log(error)
	    );
	}

	getNeighborhood(id){
		this._neighborhoodsService.getNeighborhood(id).subscribe(
	      (data) => {
	      	this.neighborhood = data
	      }
	    );
	}


	guardar() {
		this.hijo.showLoading()
		if (this.neighborhood.id == null) {
			this._neighborhoodsService.newNeighborhood(this.neighborhood)
		       	.subscribe((data) => {
		       		this.hijo.hideLoading()
	          		this.router.navigate(['/app/neighborhoods']);
	          		this.toastr.success('Barrio creado', 'Completado!');
		          
	        },
	        (error) => console.log(error));	
		}else{//actualizar
			this.neighborhood.name = this.neighborhood.name.toUpperCase();
			this._neighborhoodsService.updateNeighborhood(this.neighborhood).subscribe(
		      (data) => {
		        this.router.navigate(['/app/neighborhoods']);
		        this.toastr.success('Barrio Actualizado', 'Completado!');
		      },
		      (error) => {
		        this.toastr.error(error.msg, 'Error!');
		      }
		    );
		}
	}//end function

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//routing
import { NeighborhoodsRoutingModule } from './neighborhoods-routing.module'
//service
import { NeighborhoodsService } from './neighborhoods.service'
//components
import { NeighborhoodsComponent } from './neighborhoods.component';
import { ListNeighborhoodsComponent } from './list-neighborhoods/list-neighborhoods.component';
import { SaveNeighborhoodsComponent } from './save-neighborhoods/save-neighborhoods.component';
import { ShowLoadingComponent } from '../show-loading/show-loading.component'

@NgModule({
  declarations: [
  	NeighborhoodsComponent, 
  	ListNeighborhoodsComponent, 
  	SaveNeighborhoodsComponent,
    ShowLoadingComponent
  ],
  imports: [
    CommonModule,
    NeighborhoodsRoutingModule,
    FormsModule,
  ],
  providers: [
  	NeighborhoodsService
  ]
})
export class NeighborhoodsModule { }

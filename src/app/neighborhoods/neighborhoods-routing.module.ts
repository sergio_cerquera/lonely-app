import { RouterModule, Routes } from '@angular/router';

import { NeighborhoodsComponent } from './neighborhoods.component';


export const NeighborhoodsRoutes: Routes = [
  {
    path: 'neighborhoods',
    component: NeighborhoodsComponent,
    children: [
      { path: '', component: NeighborhoodsComponent },
      { path: 'new', component: NeighborhoodsComponent },
      { path: ':id', component: NeighborhoodsComponent },
      { path: ':id/edit', component: NeighborhoodsComponent }
    ]
  }
];
export const NeighborhoodsRoutingModule = RouterModule.forChild(NeighborhoodsRoutes);

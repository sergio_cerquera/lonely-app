import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbDate, NgbCalendar,NgbDatepickerConfig, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'lonely-front';
}

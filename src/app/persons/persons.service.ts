import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/Rx';

@Injectable()
export class PersonsService {

  personsURL: string =  environment.baseUrl + 'personController';

  constructor(private http: Http) { }

  newPerson( person ) {
    const url = `${this.personsURL}/addPerson`;
    return this.http.post( url, person )
            .map( (res) => {
              return res.json();
            });
  }

  getPersons() {
    const url = `${this.personsURL}/listPeople`;
    return this.http.get( url )
            .map( (res) => {
              return res.json();
            });
  }

  getPerson(id) {
    const url = `${this.personsURL}/${id}`;
    return this.http.get( url).map(
      (res) => res.json()
    );
  }

  updatePerson(person) {
    const url = `${this.personsURL}/${person.id}`;
    return this.http.put(url, {person}).map(
      (res) => res.json()
    );
  }

  deletePerson(id){
    const url = `${this.personsURL}/destroy_city`;
    return this.http.post( url, {id: id})
      .map( (res) => res );
  }

  searchByName(search_string: string) {
    const url = `${this.personsURL}/search`;
    return this.http.get(url, {
      params: {
        search_string
      }
    }).map(
      (res) => {
        return res.json();
      }
    );
  }

}

import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonsService } from '../persons.service'

@Component({
  selector: 'app-save-persons',
  templateUrl: './save-persons.component.html',
  styleUrls: ['./save-persons.component.css']
})
export class SavePersonsComponent implements OnInit {

	form_name:string;
	person:any = { fieldDtos: null};
	sports:any[] = []
	neighborhoods:any[] = []

	constructor(public snackBar: MatSnackBar, public _personsService: PersonsService,
		private router: Router,private route: ActivatedRoute) { 
		if (route.params["_value"].id == null) {
    	 	this.form_name = "Nueva Persona"
    	}else{
    		this.form_name = "Editar Persona"
    		route.params.subscribe(
		      (parametros) => {
		        if (parametros['id']) {
		          	this.getPerson(parametros['id']);
		        }
		      }
		    );
		} 
	}

	ngOnInit() {
	}

	getPerson(id){
		this._personsService.getPerson(id).subscribe(
	      (data) => {
	      	this.person = data
	      }
	    );
	}

	guardar() {
		if (this.person.id == null) {
			this._personsService.newPerson(this.person)
		       	.subscribe((data) => {
		          this.router.navigate(['/app/persons']);
		          this.snackBar.open('Persona creada', null, {
		          duration: 4000,
		          panelClass: 'completado',
		          horizontalPosition: 'right'
		        });
	        },
	        (error) => console.log(error));	
		}else{//actualizar
			this._personsService.updatePerson(this.person).subscribe(
		      (data) => {
		        this.router.navigate(['/app/citys']);
		        this.snackBar.open('Persona Actualizada.', null, {
		          duration: 4000,
		          panelClass: 'completado',
		          horizontalPosition: 'right'
		        });
		      },
		      (error) => {
		        this.snackBar.open(error.msg, null, {
		          duration: 4000,
		          panelClass: 'error',
		          horizontalPosition: 'right'
		        });
		      }
		    );
		}
	}//end function

}

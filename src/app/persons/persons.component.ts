import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-persons',
  template: `<router-outlet></router-outlet>`
})
export class PersonsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

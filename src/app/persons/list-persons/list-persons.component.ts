import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonsService } from '../persons.service'

@Component({
  selector: 'app-list-persons',
  templateUrl: './list-persons.component.html',
  styleUrls: ['./list-persons.component.css']
})
export class ListPersonsComponent implements OnInit {

	persons:any[] = []

	constructor(public snackBar: MatSnackBar,private router: Router,
		public _personsService: PersonsService) { 
		this.getPersons()
	}

	ngOnInit() {
	}

	getPersons(){
		this._personsService.getPersons().subscribe(
	      (data) =>{ this.persons = data},
	      (error) => console.log(error)
	    );
	}

}

import { RouterModule, Routes } from '@angular/router';

import { PersonsComponent } from './persons.component';


export const PersonsRoutes: Routes = [
  {
    path: 'persons',
    component: PersonsComponent,
    children: [
      { path: '', component: PersonsComponent },
      { path: 'new', component: PersonsComponent },
      { path: ':id', component: PersonsComponent },
      { path: ':id/edit', component: PersonsComponent }
    ]
  }
];
export const PersonsRoutingModule = RouterModule.forChild(PersonsRoutes);

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//routing
import { PersonsRoutingModule } from './persons-routing.module'
//service
import { PersonsService } from './persons.service'
//components
import { PersonsComponent } from './persons.component';
import { ListPersonsComponent } from './list-persons/list-persons.component';
import { SavePersonsComponent } from './save-persons/save-persons.component';



@NgModule({
  declarations: [PersonsComponent, ListPersonsComponent, SavePersonsComponent],
	imports: [
		CommonModule,
		PersonsRoutingModule,
		FormsModule,
	],
	providers: [
		PersonsService
	]
})
export class PersonsModule { }

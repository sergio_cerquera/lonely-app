import { Component} from '@angular/core';

@Component({
  selector: 'app-show-loading',
  templateUrl: './show-loading.component.html',
  styleUrls: ['./show-loading.component.css']
})
export class ShowLoadingComponent{

	showOverlay;

	constructor() { }

	showLoading(){
		this.showOverlay = true
	}

	hideLoading(){
		this.showOverlay = false
	}

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatButtonToggleModule, MatDatepickerModule } from '@angular/material';
//routing
import { DepartmentsRoutingModule } from './departments-routing.module'
//service
import { DepartmentsService } from './departments.service'
//components
import { DepartmentsComponent } from './departments.component';
import { ListDepartmentsComponent } from './list-departments/list-departments.component';
import { SaveDepartmentsComponent } from './save-departments/save-departments.component';

@NgModule({
  declarations: [DepartmentsComponent, ListDepartmentsComponent, SaveDepartmentsComponent],
  imports: [
    CommonModule,
    DepartmentsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatDatepickerModule
  ],
  providers: [
  	DepartmentsService
  ]
})
export class DepartmentsModule { }

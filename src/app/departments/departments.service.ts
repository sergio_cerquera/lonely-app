import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';
import { environment } from '../../environments/environment';

@Injectable()
export class DepartmentsService {

  departmentURL: string = environment.baseUrl + 'departments';

  constructor(private http: Http) { }

	newDepartment( department) {

		return this.http.post( this.departmentURL, department )
		.map( (res) => {
			return res.json();
		});
	}

	updateDepartment(department) {
		const url = `${this.departmentURL}/${department.id}`;
		return this.http.put( url, {department}).map(
			(res) => res.json()
		);
	}

	getDepartments() {
		return this.http.get( this.departmentURL)
		.map( (res) => {
		  return res.json();
		});
	}

	getDepartment(id) {
	    return this.http.get( this.departmentURL + '/' + id).map(
	      (res) => {
	        return res.json();
	      }
	    );
	}

	deleteDepartment(id){
		const url = `${this.departmentURL}/delete_department`;
		return this.http.post( url, {id: id})
		.map( (res) => res );
	}

}

import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { DepartmentsService } from '../departments.service'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-departments',
  templateUrl: './list-departments.component.html',
  styleUrls: ['./list-departments.component.css']
})
export class ListDepartmentsComponent implements OnInit {

	departments:any[] = []

	constructor(public snackBar: MatSnackBar,private router: Router,
		public _departmentsService: DepartmentsService,private toastr: ToastrService) { 
		this.getDepartments()
	}

	ngOnInit() {
	}

	getDepartments(){
		this._departmentsService.getDepartments().subscribe(
	      (data) =>{ 
	      	this.departments = data
	      },
	      (error) => console.log(error)
	    );
	}

	deleteDepartment(id){
		if(confirm("Estas seguro que deseas borrar este Departamento?")) {
		  this._departmentsService.deleteDepartment(id)
		  .subscribe( (data) => {
		    if (data["_body"] == "OK") {
				for (var i in this.departments) {
					if (this.departments[i].id == id) {
					  this.departments.splice(Number(i),1);
					}
				}
				this.toastr.success('Departamento Borrado', 'Completado!');
		    }else{
	    		this.toastr.error('No se pudo Borrar el Departamento', 'Error!');
		    }
		  });
		}
	}

}

import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { DepartmentsService } from '../departments.service'
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-save-departments',
  templateUrl: './save-departments.component.html',
  styleUrls: ['./save-departments.component.css']
})
export class SaveDepartmentsComponent implements OnInit {

	form_name:string;
	department:any = {};

	constructor(public snackBar: MatSnackBar, public _departmentsService: DepartmentsService,
		private router: Router,private route: ActivatedRoute,private toastr: ToastrService) {
		if (route.params["_value"].id == null) {
    	 	this.form_name = "Nuevo Departamento"
    	}else{
    		this.form_name = "Editar Departamento"
    		route.params.subscribe(
		      (parametros) => {
		        if (parametros['id']) {
		          	this.getDepartment(parametros['id']);
		        }
		      }
		    );
		} 
	}

	ngOnInit() {
	}

	getDepartment(id){
		this._departmentsService.getDepartment(id).subscribe(
	      (data) => {
	      	this.department = data
	      }
	    );
	}

	guardar() {
		if (this.department.id == null) {
			this._departmentsService.newDepartment(this.department)
		       	.subscribe((data) => {
		          this.router.navigate(['/app/departments']);
		          this.toastr.success('Departamento creado', 'Completado!');
	        },
	        (error) => console.log(error["_body"]));	
		}else{//actualizar
			this.department.name = this.department.name.toUpperCase();
			this._departmentsService.updateDepartment(this.department).subscribe(
		      (data) => {
		        this.router.navigate(['/app/departments']);
		        this.toastr.success('Departamento Actualizado', 'Completado!');
		      },
		      (error) => {
		      	this.toastr.error(error.msg, 'Error!');
		      }
		    );
		}
	}//end function

}

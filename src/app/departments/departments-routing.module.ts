import { RouterModule, Routes } from '@angular/router';

import { DepartmentsComponent } from './departments.component';


export const departmentsRoutes: Routes = [
  {
    path: 'departments',
    component: DepartmentsComponent,
    children: [
      { path: '', component: DepartmentsComponent },
      { path: 'new', component: DepartmentsComponent },
      { path: ':id', component: DepartmentsComponent },
      { path: ':id/edit', component: DepartmentsComponent }
    ]
  }
];
export const DepartmentsRoutingModule = RouterModule.forChild(departmentsRoutes);

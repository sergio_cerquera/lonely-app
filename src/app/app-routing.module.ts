import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LayoutComponent } from './layout/layout.component';
import { Routes, RouterModule } from '@angular/router';


const AppRoutes: Routes = [
  { path: '', redirectTo: 'app/dashboard', pathMatch: 'full' },
  //{ path: 'login', component: PageLoginComponent },
  { path: 'app', component: LayoutComponent },
  { path: '**', redirectTo: 'app/dashboard', pathMatch: 'full' }
];

export const AppRoutingModule = RouterModule.forRoot(AppRoutes, {useHash: true});

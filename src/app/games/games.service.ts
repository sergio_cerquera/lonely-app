import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/Rx';

@Injectable()
export class GamesService {

  gamesURL: string =  environment.baseUrl + 'gameController';

  constructor(private http: Http) { }

  newGame( game ) {
    const url = `${this.gamesURL}/addGame`;
    return this.http.post( url, game )
    .map( (res) => {
      return res.json();
    });
  }

  getGames() {
    const url = `${this.gamesURL}/listNeighborhoods`;
    return this.http.get( url )
            .map( (res) => {
              return res.json();
            });
  }

  setPersonToGame(person, codeGame){
    const url = `${this.gamesURL}/addPersonToGame`;
    return this.http.get( url,{
        params: {
          person,
          codeGame
        }
      })
      .map( (res) => {
        return res.json();
      });
  }

  getGame(id) {
    const url = `${this.gamesURL}/${id}`;
    return this.http.get( url).map(
      (res) => res.json()
    );
  }

  updateGame(game) {
    const url = `${this.gamesURL}/${game.id}`;
    return this.http.put(url, {game}).map(
      (res) => res.json()
    );
  }

  deleteGame(id){
    const url = `${this.gamesURL}/destroy_game`;
    return this.http.post( url, {id: id})
      .map( (res) => res );
  }

  searchByName(search_string: string) {
    const url = `${this.gamesURL}/search`;
    return this.http.get(url, {
      params: {
        search_string
      }
    }).map(
      (res) => {
        return res.json();
      }
    );
  }

}

import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { GamesService } from '../games.service'

@Component({
  selector: 'app-list-games',
  templateUrl: './list-games.component.html',
  styleUrls: ['./list-games.component.css']
})
export class ListGamesComponent implements OnInit {

	games:any[] = []

	constructor(public snackBar: MatSnackBar,private router: Router,
		public _gamesService: GamesService) { 
		this.getGames()
	}

	ngOnInit() {
	}

	getGames(){
		this._gamesService.getGames().subscribe(
	      (data) =>{ this.games = data},
	      (error) => console.log(error)
	    );
	}

}

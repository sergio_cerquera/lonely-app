import { RouterModule, Routes } from '@angular/router';

import { GamesComponent } from './games.component';


export const GamesRoutes: Routes = [
  {
    path: 'games',
    component: GamesComponent,
    children: [
      { path: '', component: GamesComponent },
      { path: 'new', component: GamesComponent },
      { path: ':id', component: GamesComponent },
      { path: ':id/edit', component: GamesComponent }
    ]
  }
];
export const GamesRoutingModule = RouterModule.forChild(GamesRoutes);

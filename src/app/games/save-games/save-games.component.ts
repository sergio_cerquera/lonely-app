import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { GamesService } from '../games.service'
import { FieldsService } from '../../fields/fields.service';
import { PersonsService } from '../../persons/persons.service'
import { NeighborhoodsService } from '../../neighborhoods/neighborhoods.service'
import { NgbDate, NgbCalendar,NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-save-games',
  templateUrl: './save-games.component.html',
  styleUrls: ['./save-games.component.css']
})
export class SaveGamesComponent implements OnInit {

	form_name:string;
	game:any = {field: ""};
	game_details:any[] = []
	fields:any[] = []
	persons:any[] = []
	neighborhoods:any[] = []

	constructor(public snackBar: MatSnackBar, public _gamesService: GamesService,
		private router: Router,private route: ActivatedRoute,
		public _fieldsService: FieldsService, public _personsService: PersonsService,
		public _neighborhoodsService: NeighborhoodsService,config: NgbDatepickerConfig) { 
		this.getFields();
		this.getPersons()
		if (route.params["_value"].id == null) {
    	 	this.form_name = "Nuevo Juego"
    	 	this.addGameDetail();
    	}else{
    		this.form_name = "Editar Juego"
    		route.params.subscribe(
		      (parametros) => {
		        if (parametros['id']) {
		          	this.getGame(parametros['id']);
		        }
		      }
		    );
		} 
	}

	ngOnInit() {
	}

	getGame(id){
		this._gamesService.getGame(id).subscribe(
	      (data) => {
	      	this.game = data
	      }
	    );
	}

	getFields(){
		this._neighborhoodsService.getNeighborhoods().subscribe(
	      (data) =>{ 
	      	this.neighborhoods = data
	      	if (this.neighborhoods.length > 0) {
	      		this.neighborhoods.forEach( (neighborhood, i) => {
			        if (neighborhood.fields.length > 0) {
			        	neighborhood.fields.forEach( (field, i) => {
			        		this.fields.push(field)
			        	});
			        }
		      	});
	      	}
	      },
	      (error) => console.log(error)
	    );
	}

	getPersons(){
		this._personsService.getPersons().subscribe(
	      (data) =>{ this.persons = data},
	      (error) => console.log(error)
	    );
	}

	addGameDetail() {
      let e: any;
      e = {
        game_id: this.game.id,
        person: "",
        person_dni: '',
        person_name: '',
        isEditable: true,
      };
      this.game_details.push(e);
    }

    getPersonName(game_detail){
    	this.persons.forEach( (person, i) => {
	        if (person.name == game_detail.person) {
	        	game_detail.person_dni = person.dni
	        	game_detail.person_name = person.name
	        }
      	});
    }

	guardar() {
		this.setDatesFormat()
		if (this.game.id == null) {
			this._gamesService.newGame(this.game)
		       	.subscribe((data) => {
		       		this.addPersonToGame()
					this.router.navigate(['/app/games']);
					this.snackBar.open('Juego creado', null, {
					duration: 4000,
					panelClass: 'completado',
					horizontalPosition: 'right'
		        });
	        },
	        (error) => console.log(error));	
		}else{//actualizar
			this._gamesService.updateGame(this.game).subscribe(
		      (data) => {
		        this.router.navigate(['/app/games']);
		        this.snackBar.open('Juego Actualizado.', null, {
		          duration: 4000,
		          panelClass: 'completado',
		          horizontalPosition: 'right'
		        });
		      },
		      (error) => {
		        this.snackBar.open(error.msg, null, {
		          duration: 4000,
		          panelClass: 'error',
		          horizontalPosition: 'right'
		        });
		      }
		    );
		}
	}//end function


	addPersonToGame(){
		this.game_details.forEach( (detail, i) => {
			this._gamesService.setPersonToGame(detail.person,this.game.code).subscribe(
		      (data) => {
		        
		      },
		      (error) => {
		        this.snackBar.open(error.msg, null, {
		          duration: 4000,
		          panelClass: 'error',
		          horizontalPosition: 'right'
		        });
		      }
		    );
      	});
	}

	setDatesFormat(){
		if (this.game.date != null) {
		  let fecha = this.game.date
		  let new_fecha = new Date(fecha.year, fecha.month -1 , fecha.day);
		  this.game.date = new_fecha  
		}
	}

	setNgbFormat(){
		if (this.game.date != null) {
		  let fecha = new Date(this.game.date);
		  let new_fecha = new NgbDate(fecha.getFullYear(), fecha.getMonth() + 1, fecha.getDate()+1)
		  this.game.date = new_fecha  
		}
	}//end function

}

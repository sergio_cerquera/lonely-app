import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatButtonModule, MatButtonToggleModule, MatDatepickerModule } from '@angular/material';

//routing
import { GamesRoutingModule } from './games-routing.module'
//service
import { GamesService } from './games.service'
//components
import { GamesComponent } from './games.component';
import { ListGamesComponent } from './list-games/list-games.component';
import { SaveGamesComponent } from './save-games/save-games.component';



@NgModule({
  declarations: [GamesComponent, ListGamesComponent, SaveGamesComponent],
  imports: [
    CommonModule,
    GamesRoutingModule,
    FormsModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatDatepickerModule,
    NgbModule
  ],
  providers: [
  	GamesService
  ]

})
export class GamesModule { }

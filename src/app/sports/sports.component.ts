import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sports',
  template: `<router-outlet></router-outlet>`
})
export class SportsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

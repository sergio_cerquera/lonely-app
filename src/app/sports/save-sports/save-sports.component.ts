import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { SportsService } from '../sports.service'
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-save-sports',
  templateUrl: './save-sports.component.html',
  styleUrls: ['./save-sports.component.css']
})
export class SaveSportsComponent implements OnInit {

	form_name:string;
	sport:any = {};

	constructor(public snackBar: MatSnackBar, public _sportsService: SportsService,
		private router: Router,private route: ActivatedRoute,private toastr: ToastrService) {
		if (route.params["_value"].id == null) {
    	 	this.form_name = "Nueva Deporte"
    	}else{
    		this.form_name = "Editar Deporte"
    		route.params.subscribe(
		      (parametros) => {
		        if (parametros['id']) {
		          	this.getSport(parametros['id']);
		        }
		      }
		    );
		} 
	}

	ngOnInit() {
	}

	getSport(id){
		this._sportsService.getSport(id).subscribe(
	      (data) => {
	      	this.sport = data
	      }
	    );
	}


	guardar() {
		if (this.sport.id == null) {
			this._sportsService.newSport(this.sport)
		       	.subscribe((data) => {
		          this.router.navigate(['/app/sports']);
		          this.toastr.success('Deporte creado', 'Completado!');
	        },
	        (error) => console.log(error));	
		}else{//actualizar
			this.sport.name = this.sport.name.toUpperCase();
			this._sportsService.updateSport(this.sport).subscribe(
		      (data) => {
		        this.router.navigate(['/app/sports']);
		        this.toastr.success('Deporte Actualizado', 'Completado!');
		      },
		      (error) => {
		        this.toastr.error(error.msg, 'Error!');
		      }
		    );
		}
	}//end function
}

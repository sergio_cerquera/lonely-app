import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { SportsService } from '../sports.service'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-sports',
  templateUrl: './list-sports.component.html',
  styleUrls: ['./list-sports.component.css']
})
export class ListSportsComponent implements OnInit {

	sports:any[] = []

	constructor(public snackBar: MatSnackBar,
		private router: Router,public _sportsService: SportsService,private toastr: ToastrService) { 
		this.getSports();
	}

	ngOnInit() {
	}

	getSports(){
		this._sportsService.getSports().subscribe(
	      (data) =>{ this.sports = data},
	      (error) => console.log(error)
	    );
	}

	deleteSport(id){
		if(confirm("Estas seguro que deseas borrar este Deporte?")) {
		  this._sportsService.deleteSport(id)
		  .subscribe( (data) => {
		    if (data["_body"] == "OK") {
				for (var i in this.sports) {
					if (this.sports[i].id == id) {
					  this.sports.splice(Number(i),1);
					}
				}
				this.toastr.success('Deporte Borrado', 'Completado!');
		    }else{
	    		this.toastr.error('No se pudo Borrar el Deporte', 'Error!');
		    }
		  });
		}

}

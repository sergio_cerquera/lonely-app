import { RouterModule, Routes } from '@angular/router';

import { SportsComponent } from './sports.component';


export const SportsRoutes: Routes = [
  {
    path: 'sports',
    component: SportsComponent,
    children: [
      { path: '', component: SportsComponent },
      { path: 'new', component: SportsComponent },
      { path: ':id', component: SportsComponent },
      { path: ':id/edit', component: SportsComponent }
    ]
  }
];
export const SportsRoutingModule = RouterModule.forChild(SportsRoutes);

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/Rx';

@Injectable()
export class SportsService {

  sportsURL: string =  environment.baseUrl + 'sports';

  constructor(private http: Http) { }

  newSport( sport ) {
    const url = `${this.sportsURL}`;
    return this.http.post( url, sport )
            .map( (res) => {
              return res.json();
            });
  }

  getSports() {
    const url = `${this.sportsURL}`;
    return this.http.get( url )
            .map( (res) => {
              return res.json();
            });
  }

  getSport(id) {
    const url = `${this.sportsURL}/${id}`;
    return this.http.get( url).map(
      (res) => res.json()
    );
  }

  updateSport(sport) {
    const url = `${this.sportsURL}/${sport.id}`;
    return this.http.put(url, {sport}).map(
      (res) => res.json()
    );
  }

  deleteSport(id){
    const url = `${this.sportsURL}/delete_sport`;
    return this.http.post( url, {id: id}) 
  }

  searchByName(search_string: string) {
    const url = `${this.sportsURL}/search`;
    return this.http.get(url, {
      params: {
        search_string
      }
    }).map(
      (res) => {
        return res.json();
      }
    );
  }

}

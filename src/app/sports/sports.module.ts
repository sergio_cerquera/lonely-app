import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//service
import { SportsService } from './sports.service'
//routing
import { SportsRoutingModule } from './sports-routing.module'
//components
import { SportsComponent } from './sports.component';
import { ListSportsComponent } from './list-sports/list-sports.component';
import { SaveSportsComponent } from './save-sports/save-sports.component'

@NgModule({
  imports: [
    CommonModule,
    SportsRoutingModule,
    FormsModule,
  ],
  declarations: [
    SportsComponent,
    ListSportsComponent,
    SaveSportsComponent
  ],
  providers: [
    SportsService
  ],
})
export class SportsModule { }

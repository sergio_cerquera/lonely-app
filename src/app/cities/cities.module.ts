import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//routing
import { CitiesRoutingModule } from './cities-routing.module'
//service
import { CitiesService } from './cities.service'
//components
import { CitiesComponent } from './cities.component';
import { ListCitiesComponent } from './list-cities/list-cities.component';
import { SaveCitiesComponent } from './save-cities/save-cities.component'

@NgModule({
  declarations: [
  	CitiesComponent,
  	ListCitiesComponent,
  	SaveCitiesComponent
  ],
  imports: [
    CommonModule,
    CitiesRoutingModule,
    FormsModule,
	ReactiveFormsModule
  ],
  providers: [
  	CitiesService
  ]
})
export class CitiesModule { }

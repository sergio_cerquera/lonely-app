import { RouterModule, Routes } from '@angular/router';

import { CitiesComponent } from './cities.component';


export const CitiesRoutes: Routes = [
  {
    path: 'cities',
    component: CitiesComponent,
    children: [
      { path: '', component: CitiesComponent },
      { path: 'new', component: CitiesComponent },
      { path: ':id', component: CitiesComponent },
      { path: ':id/edit', component: CitiesComponent }
    ]
  }
];
export const CitiesRoutingModule = RouterModule.forChild(CitiesRoutes);

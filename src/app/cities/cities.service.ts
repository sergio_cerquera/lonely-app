import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http'
import { environment } from '../../environments/environment';
import 'rxjs/Rx';

@Injectable()
export class CitiesService {

  citiesURL: string =  environment.baseUrl + 'cities';

  constructor(private http: Http, private https: HttpClient) { }

  newCity( city) {
    return this.http.post( this.citiesURL, city )
    .map( (res) => {
      return res.json();
    });
  }

  updateCity(city) {
    const url = `${this.citiesURL}/${city.id}`;
    return this.http.put( url, {city}).map(
      (res) => res.json()
    );
  }

  getCities() {
    return this.http.get( this.citiesURL)
    .map( (res) => {
      return res.json();
    });
  }

  getCity(id) {
      return this.http.get( this.citiesURL + '/' + id).map(
        (res) => {
          return res.json();
        }
      );
  }

  deleteCity(id){
    const url = `${this.citiesURL}/delete_city`;
    return this.http.post( url, {id: id})
    .map( (res) => res );
  }

}

import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { CitiesService } from '../cities.service'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-cities',
  templateUrl: './list-cities.component.html',
  styleUrls: ['./list-cities.component.css']
})
export class ListCitiesComponent implements OnInit {

	cities:any[] = []

	constructor(public snackBar: MatSnackBar,private router: Router,
		public _citiesService: CitiesService,private toastr: ToastrService) { 
		this.getCities()
	}

	ngOnInit() {
	}

	getCities(){
		this._citiesService.getCities().subscribe(
	      (data) =>{ 
	      	this.cities = data
	      },
	      (error) => console.log(error)
	    );
	}

	deleteCity(id){
		if(confirm("Estas seguro que deseas borrar esta Ciudad?")) {
		  this._citiesService.deleteCity(id)
		  .subscribe( (data) => {
		    if (data["_body"] == "OK") {
				for (var i in this.cities) {
					if (this.cities[i].id == id) {
					  this.cities.splice(Number(i),1);
					}
				}
				this.toastr.success('Ciudad Borrada', 'Completado!');
		    }else{
	    		this.toastr.error('No se pudo Borrar la Ciudad', 'Error!');
		    }
		  });
		}
	}

}

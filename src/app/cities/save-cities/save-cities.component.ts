import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { CitiesService } from '../cities.service'
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DepartmentsService } from '../../departments/departments.service'

@Component({
  selector: 'app-save-cities',
  templateUrl: './save-cities.component.html',
  styleUrls: ['./save-cities.component.css']
})
export class SaveCitiesComponent implements OnInit {

	form_name:string;
	city:any = {departament_id: ""};
	departments:any[] = []

	constructor(public snackBar: MatSnackBar, public _citiesService: CitiesService,
		private router: Router,private route: ActivatedRoute,private toastr: ToastrService,
		private _departmentsService: DepartmentsService) {
		this.getDepartments()
		if (route.params["_value"].id == null) {
    	 	this.form_name = "Nueva Ciudad"
    	}else{
    		this.form_name = "Editar Ciudad"
    		route.params.subscribe(
		      (parametros) => {
		        if (parametros['id']) {
		          	this.getCity(parametros['id']);
		        }
		      }
		    );
		} 
	}

	ngOnInit() {
	}

	getDepartments(){
		this._departmentsService.getDepartments().subscribe(
	      (data) =>{ 
	      	this.departments = data
	      },
	      (error) => console.log(error)
	    );
	}

	getCity(id){
		this._citiesService.getCity(id).subscribe(
	      (data) => {
	      	this.city = data
	      }
	    );
	}


	guardar() {
		if (this.city.id == null) {
			this._citiesService.newCity(this.city)
		       	.subscribe((data) => {
		          this.router.navigate(['/app/cities']);
		          this.toastr.success('Ciudad creada', 'Completado!');
	        },
	        (error) => console.log(error));	
		}else{//actualizar
			this.city.name = this.city.name.toUpperCase();
			this._citiesService.updateCity(this.city).subscribe(
		      (data) => {
		        this.router.navigate(['/app/cities']);
		        this.toastr.success('Ciudad Actualizada.', 'Completado!');
		      },
		      (error) => {
		        this.toastr.error(error.msg, 'Error!');
		      }
		    );
		}
	}//end function

}

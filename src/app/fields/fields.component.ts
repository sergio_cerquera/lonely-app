import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fields',
  template: `<router-outlet></router-outlet>`
})
export class FieldsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

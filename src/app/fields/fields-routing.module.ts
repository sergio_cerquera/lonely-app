import { RouterModule, Routes } from '@angular/router';

import { FieldsComponent } from './fields.component';


export const FieldsRoutes: Routes = [
  {
    path: 'fields',
    component: FieldsComponent,
    children: [
      { path: '', component: FieldsComponent },
      { path: 'new', component: FieldsComponent },
      { path: ':id', component: FieldsComponent },
      { path: ':id/edit', component: FieldsComponent }
    ]
  }
];
export const FieldsRoutingModule = RouterModule.forChild(FieldsRoutes);

import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { FieldsService } from '../fields.service'
import { NeighborhoodsService } from '../../neighborhoods/neighborhoods.service'

@Component({
  selector: 'app-list-fields',
  templateUrl: './list-fields.component.html',
  styleUrls: ['./list-fields.component.css']
})
export class ListFieldsComponent implements OnInit {

	fields:any[] = []

	neighborhoods:any[] = []

	constructor(public snackBar: MatSnackBar,private router: Router,
		public _fieldsService: FieldsService, private _neighborhoodsService: NeighborhoodsService) { 
		//this.getFields()
		this.getNeighborhoods();
	}

	ngOnInit() {
	}

	getFields(){
		this._fieldsService.getFields().subscribe(
	      (data) =>{ this.fields = data},
	      (error) => console.log(error)
	    );
	}

	getNeighborhoods(){
		this._neighborhoodsService.getNeighborhoods().subscribe(
	      (data) =>{ 
	      	this.neighborhoods = data
	      	if (this.neighborhoods.length > 0) {
	      		this.neighborhoods.forEach( (neighborhood, i) => {
			        if (neighborhood.fields.length > 0) {
			        	neighborhood.fields.forEach( (field, i) => {
			        		this.fields.push(field)
			        	});
			        }
		      	});
	      	}
	      	console.log(this.fields)
	      },
	      (error) => console.log(error)
	    );
	}

}

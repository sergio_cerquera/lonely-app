import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { FieldsService } from '../fields.service';
import { NeighborhoodsService } from '../../neighborhoods/neighborhoods.service'
import { SportsService } from '../../sports/sports.service'

@Component({
  selector: 'app-save-fields',
  templateUrl: './save-fields.component.html',
  styleUrls: ['./save-fields.component.css']
})
export class SaveFieldsComponent implements OnInit {

	form_name:string;
	neighborhoods:any[] = []
	field:any = {};
	sports:any[] = []

	constructor(public snackBar: MatSnackBar, public _neighborhoodsService: NeighborhoodsService,
		private router: Router,private route: ActivatedRoute,
		private _fieldsService: FieldsService, private _sportsService: SportsService) {
		this.getNeighborhoods();
		this.getSports()
		if (route.params["_value"].id == null) {
    	 	this.form_name = "Nueva Cancha"
    	}else{
    		this.form_name = "Editar Cancha"
    		route.params.subscribe(
		      (parametros) => {
		        if (parametros['id']) {
		          	this.getField(parametros['id']);
		        }
		      }
		    );
		} 
	}

	ngOnInit() {
	}

	getField(id){
		this._fieldsService.getField(id).subscribe(
	      (data) => {
	      	this.field = data
	      }
	    );
	}

	getNeighborhoods(){
		this._neighborhoodsService.getNeighborhoods().subscribe(
	      (data) =>{ this.neighborhoods = data},
	      (error) => console.log(error)
	    );
	}

	getSports(){
		this._sportsService.getSports().subscribe(
	      (data) =>{ this.sports = data},
	      (error) => console.log(error)
	    );
	}

	guardar() {
		if (this.field.id == null) {
			this._fieldsService.newField(this.field)
		       	.subscribe((data) => {
		          this.router.navigate(['/app/fields']);
		          this.snackBar.open('Cancha creada', null, {
		          duration: 4000,
		          panelClass: 'completado',
		          horizontalPosition: 'right'
		        });
	        },
	        (error) => console.log(error));	
		}else{//actualizar
			this._fieldsService.updateField(this.field).subscribe(
		      (data) => {
		        this.router.navigate(['/app/fieds']);
		        this.snackBar.open('Cancha Actualizada.', null, {
		          duration: 4000,
		          panelClass: 'completado',
		          horizontalPosition: 'right'
		        });
		      },
		      (error) => {
		        this.snackBar.open(error.msg, null, {
		          duration: 4000,
		          panelClass: 'error',
		          horizontalPosition: 'right'
		        });
		      }
		    );
		}
	}//end function

}

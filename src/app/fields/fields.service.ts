import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/Rx';

@Injectable()
export class FieldsService {

  fieldsURL: string =  environment.baseUrl + 'fieldController';
  neighborhoodsURL: string =  environment.baseUrl + 'neighborhoodController';

  constructor(private http: Http) { }

  newField( field ) {
    const url = `${this.neighborhoodsURL}/addField`;
    return this.http.post( url, field )
            .map( (res) => {
              return res.json();
            });
  }

  getFields() {
    const url = `${this.fieldsURL}/listFields`;
    return this.http.get( this.fieldsURL )
            .map( (res) => {
              return res.json();
            });
  }

  getField(id) {
    const url = `${this.fieldsURL}/${id}`;
    return this.http.get( url).map(
      (res) => res.json()
    );
  }

  updateField(field) {
    const url = `${this.fieldsURL}/${field.id}`;
    return this.http.put(url, {field}).map(
      (res) => res.json()
    );
  }

  deleteField(id){
    const url = `${this.fieldsURL}/destroy_city`;
    return this.http.post( url, {id: id})
      .map( (res) => res );
  }

  searchByName(search_string: string) {
    const url = `${this.fieldsURL}/search`;
    return this.http.get(url, {
      params: {
        search_string
      }
    }).map(
      (res) => {
        return res.json();
      }
    );
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//routing
import { FieldsRoutingModule } from './fields-routing.module'
//service
import { FieldsService } from './fields.service'
//components
import { FieldsComponent } from './fields.component';
import { ListFieldsComponent } from './list-fields/list-fields.component';
import { SaveFieldsComponent } from './save-fields/save-fields.component';



@NgModule({
  declarations: [
  	FieldsComponent,
  	ListFieldsComponent,
  	SaveFieldsComponent
  ],
  imports: [
    CommonModule,
    FieldsRoutingModule,
    FormsModule,
  ],
  providers: [
  	FieldsService
  ]
})
export class FieldsModule { }

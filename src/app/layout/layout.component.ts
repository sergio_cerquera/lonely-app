import { Component, OnInit } from '@angular/core';
import {
  Router,
  Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from '@angular/router'

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

	public showOverlay = true;

	constructor(private router: Router) { 
		router.events.subscribe((event: RouterEvent) => {
	      this.navigationInterceptor(event)
	    })
	}

	navigationInterceptor(event: RouterEvent): void {
	    if (event instanceof NavigationStart) {
	    	console.log(event)
	      this.showOverlay = true;
	    }
	    if (event instanceof NavigationEnd) {
		     setTimeout(() => (this.showOverlay = false),300);
		}

	    // Set loading state to false in both of the below events to hide the spinner in case a request fails
	    if (event instanceof NavigationCancel) {
	      this.showOverlay = false;
	    }
	    if (event instanceof NavigationError) {
	      this.showOverlay = false;
	    }
	    localStorage.setItem("show_loading",this.showOverlay.toString());
	}



	ngOnInit() {
	}

}

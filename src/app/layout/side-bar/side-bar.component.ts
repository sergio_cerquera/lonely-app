import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';// import Jquery here   

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

	public showOverlay = true;
	
	clicked:number = 0
	string_aux:string = ""

	constructor() { }

	ngOnInit() {
	}

	hideMenu2(id){
		console.log(id)
		console.log(this.string_aux)
		if (this.string_aux != id) {
			if (this.string_aux != "") {
				setTimeout(() => ($("#"+this.string_aux).toggle(false)),300);
			}
			setTimeout(() => ($("#"+id).toggle()),300);
			this.string_aux = id
		}else{
			setTimeout(() => ($("#"+id).toggle()),300);
		}
		//setTimeout(() => (this.showOverlay = false),300);
		//console.log($("#"+id).toggle())
	}

	hideMenu(id){
		$( ".collapse" ).each(function( index ) {
			if ($(this).attr("id") == id) {
				setTimeout(() => ($("#"+id).toggle()),200);
			}else{
				$(this).attr("class","collapse")
				$(".collapse").toggle(false);
			}
		});
	}

}

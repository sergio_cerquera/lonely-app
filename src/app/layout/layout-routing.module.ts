import { RouterModule, Routes } from '@angular/router';
//components
import { LayoutComponent } from './layout.component';
import { DashboardComponent } from '../dashboard/dashboard.component'
//sports routes
import { SportsComponent } from '../sports/sports.component'
import { ListSportsComponent } from '../sports/list-sports/list-sports.component'
import { SaveSportsComponent } from '../sports/save-sports/save-sports.component'
//Cities routes
import { CitiesComponent } from '../cities/cities.component'
import { ListCitiesComponent } from '../cities/list-cities/list-cities.component'
import { SaveCitiesComponent } from '../cities/save-cities/save-cities.component'
//Neighborhoods routes
import { NeighborhoodsComponent } from '../neighborhoods/neighborhoods.component'
import { ListNeighborhoodsComponent } from '../neighborhoods/list-neighborhoods/list-neighborhoods.component'
import { SaveNeighborhoodsComponent } from '../neighborhoods/save-neighborhoods/save-neighborhoods.component'
//Fields routes
import { FieldsComponent } from '../fields/fields.component'
import { ListFieldsComponent } from '../fields/list-fields/list-fields.component'
import { SaveFieldsComponent } from '../fields/save-fields/save-fields.component'
//Persons routes
import { PersonsComponent } from '../persons/persons.component'
import { ListPersonsComponent } from '../persons/list-persons/list-persons.component'
import { SavePersonsComponent } from '../persons/save-persons/save-persons.component'
//Games routes
import { GamesComponent } from '../games/games.component'
import { ListGamesComponent } from '../games/list-games/list-games.component'
import { SaveGamesComponent } from '../games/save-games/save-games.component'
//departments routes
import { DepartmentsComponent } from '../departments/departments.component'
import { ListDepartmentsComponent } from '../departments/list-departments/list-departments.component'
import { SaveDepartmentsComponent } from '../departments/save-departments/save-departments.component'


const routes: Routes = [
  {
    path: 'app',
    component: LayoutComponent,
    children: [
		{ path: '', redirectTo: '/app/dashboard', pathMatch: 'full' },
		{ path: 'dashboard', component: DashboardComponent },
		{
	        path: 'sports',
	        component: SportsComponent,
	        children: [
	          { path: '', component: ListSportsComponent },
	          { path: 'new', component: SaveSportsComponent },
	          { path: ':id/edit', component: SaveSportsComponent }
	        ]
	    },
	    {
	        path: 'departments',
	        component: DepartmentsComponent,
	        children: [
	          { path: '', component: ListDepartmentsComponent },
	          { path: 'new', component: SaveDepartmentsComponent },
	          { path: ':id/edit', component: SaveDepartmentsComponent }
	        ]
	    },
      	{
	        path: 'cities',
	        component: CitiesComponent,
	        children: [
	          { path: '', component: ListCitiesComponent },
	          { path: 'new', component: SaveCitiesComponent },
	          { path: ':id/edit', component: SaveCitiesComponent }
	        ]
	    },
	    {
	        path: 'neighborhoods',
	        component: NeighborhoodsComponent,
	        children: [
	          { path: '', component: ListNeighborhoodsComponent },
	          { path: 'new', component: SaveNeighborhoodsComponent },
	          { path: ':id/edit', component: SaveNeighborhoodsComponent }
	        ]
	    },
	    {
	        path: 'fields',
	        component: FieldsComponent,
	        children: [
	          { path: '', component: ListFieldsComponent },
	          { path: 'new', component: SaveFieldsComponent },
	          { path: ':id/edit', component: SaveFieldsComponent }
	        ]
	    },
	    {
	        path: 'persons',
	        component: PersonsComponent,
	        children: [
	          { path: '', component: ListPersonsComponent },
	          { path: 'new', component: SavePersonsComponent },
	          { path: ':id/edit', component: SavePersonsComponent }
	        ]
	    },
	    {
	        path: 'games',
	        component: GamesComponent,
	        children: [
	          { path: '', component: ListGamesComponent },
	          { path: 'new', component: SaveGamesComponent },
	          { path: ':id/edit', component: SaveGamesComponent }
	        ]
	    },
    ]
  }
];

export const LayoutRoutingModule = RouterModule.forChild(routes);
